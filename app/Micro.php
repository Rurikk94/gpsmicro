<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Micro extends Model
{
    protected $fillable = [
        'name', 'color', 'number'
    ];
}
