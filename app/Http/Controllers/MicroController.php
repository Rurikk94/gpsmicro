<?php

namespace App\Http\Controllers;

use App\Micro;
use Illuminate\Http\Request;

class MicroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $micros = Micro::latest()->paginate(5); 

        return view('micros.index',compact('micros'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('micros.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'color' => 'required',
            'number' => 'required',
        ]);

        Micro::create($request->all());
        return redirect()->route('micros.index')
            ->with('success','Micro created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function show(Micro $micro)
    {
        return view('micros.show',compact('micro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function edit(Micro $micro)
    {
        return view('micros.edit',compact('micro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Micro $micro)
    {
        $request->validate([
            'name' => 'required',
            'color' => 'required',
            'number' => 'required',
        ]); 

        $micro->update($request->all());
        return redirect()->route('micros.index')
            ->with('success','Micro updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Micro  $micro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Micro $micro)
    {
        $micro->delete();
        
        return redirect()->route('micros.index')
            ->with('success','Micro deleted successfully');
    }
}
