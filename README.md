# GpsMicros

Sistema Web para la visualización, en tiempo real,  en un mapa de la ubicación de la locomoción.

## Características

- El usuario, podrá compartir su ubicación actual cuando esté en la micro, para que otros usuarios sepa donde va la micro.
- El sistema, tomará la ubicación y la reubicará en la ruta de la micro, para corregir errores del gps.
- Si el usuario, necesita saber la ubicación de determinada micro, buscará en el filtro de búsqueda, y el sistema mostrará solo la ubicación de las micros del recorrido escogido.
- Al compartir una ubicación, esta se actualizará cada 30 segundos.

## Requisitos Técnicos

Laravel 6

Php 7.2

MySQL 5.7

Apache 2.4